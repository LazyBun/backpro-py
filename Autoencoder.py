import random as rand
from math import exp
from math import tanh
from math import sqrt
from math import log
from PIL import Image
import numpy as np
import os as os
import matplotlib.pyplot as plt

p_roof = []
beta = -.1
p = 0.05
network_neurons = 50
max_epochs = 200
eta = 0.2
training_set = "nums_big"

def fifoRand_tanh(input_cout, output_count):
    return rand.uniform(-1 * sqrt(6/(input_cout + output_count)), sqrt(6/(input_cout + output_count)))

def fifoRand_sigm(input_cout, output_count):
    return rand.uniform(-4 * sqrt(6/(input_cout + output_count)), 4 * sqrt(6/(input_cout + output_count)))


# Init network with biases
def init(input_count, hidden_count, output_count):
    network = []
    hidden_layer = [{'weights': [fifoRand_sigm(input_count, output_count) for i in range(input_count + 1)]} for i in range(hidden_count)]
    network.append(hidden_layer)
    output_layer = [{'weights': [fifoRand_sigm(input_count, output_count) for i in range(hidden_count + 1)]} for i in range(output_count)]
    network.append(output_layer)
    return network


# Forward
# Activate - sum(input * weights) + bias
def activate(input, weights):
    activation = 0
    for i in range(len(weights) - 1):
        activation += input[i] * weights[i]
    return activation + weights[-1]


def transfer_activation_sigm(activation):
    return 1.0 / (1.0 + (exp(-activation)))


# def transfer_activation_tanh(activation):
#     return (exp(activation) - exp(-activation)) / (exp(activation) + exp(-activation))
def transfer_activation_tanh(activation):
    return (2/(1.0 + exp(-2*activation))) - 1.0


def forward_propagation(network, input):
    for layer in network:
        input = forward_propagation_step(layer, input)
    return input


def forward_propagation_step(layer, input):
    next_input = []
    for node in layer:
        node['activation'] = activate(input, node['weights'])
        node['output'] = transfer_activation_sigm(node['activation'])
        next_input.append(node['output'])
    return next_input




# Backpropagate

# derivative
def derivative_sigm(output):
    return output * (1.0 - output)

def derivative_tanh(output):
    return 1.0 - (tanh(output) ** 2)


# Progagate error
# error = (expected - output) * transfer_derivative(output) for output
# error = (weight_k * error_j) * transfer_derivative(output) for further layers

#http://stats.stackexchange.com/questions/154879/a-list-of-cost-functions-used-in-neural-networks-alongside-applications
#mean squared error
def mse(a, e):
    return a - e

def backpropagate_mse(network, expected_result):
    for n in range(len(network)):
        i = -n - 1
        layer = network[i]
        if i == -1:  # 1st layer
            for j in range(len(layer)):
                layer[j]['delta'] = mse(expected_result[j], layer[j]['output']) * derivative_sigm(layer[j]['output'])
        else:
            for j in range(len(layer)):
                regularization = beta * ((-1 * (p / p_roof[j])) + ((1 - p) / (1 - p_roof[j])))
                error = 0.0
                for node in network[i + 1]:
                    error += node['weights'][j] * node['delta']
                layer[j]['delta'] = (error + regularization) * derivative_sigm(layer[j]['output'])



# Training

# Update
def update_weights(network, inputs, eta):
    for i in range(len(network)):
        if i != 0:
            inputs = [neuron['output'] for neuron in network[i - 1]]
        for neuron in network[i]:
            for j in range(len(inputs) - 1):
                neuron['weights'][j] += eta * neuron['delta'] * inputs[j]
            neuron['weights'][-1] += eta * neuron['delta']


# Train
def train(network, training_set, eta, epochs_count, outputs_count):
    sum_error = 0
    for epoch in range(epochs_count):
        previous_err = sum_error
        sum_error = 0
        #compute p_roof
        global p_roof
        p_roof = [0 for i in range(len(network[0]))]
        for row in training_set:
            forward_propagation(network, row)
            for i in range(len(network[0])):
                p_roof[i] += (network[0][i]['activation'])
        p_roof = [item/(len(training_set))for item in p_roof]
        for row in training_set:
            outputs = forward_propagation(network, row)
            expected = row
            sum_error += 1/2 * sum([((expected[i] - outputs[i]) ** 2) for i in range(len(expected) - 1)])
            backpropagate_mse(network, expected)
            update_weights(network, row, eta)
            # print(outputs)
        # test(network)
        print('epoch:%d -- err:%.10f' % (epoch, sum_error/len(training_set)))


def predict(network, row):
    outputs = forward_propagation(network, row)
    return outputs


def parse_imgs(path):
    nums = []
    for filename in os.listdir(path):
        if filename.endswith(".png"):
            numOnImg = int(filename.split(".")[0][0])
            img = np.array(Image.open(path + "/" + filename))
            thisNum = [(float(img[i][j][0])/255) for i in range(img.shape[0]) for j in range(img.shape[1])]
            thisNum.append(numOnImg)
            nums.append(thisNum)
    return nums



def random_swap(imgs):
    result = []
    for img in imgs:
        result.append([1.0 - img[i] if rand.random() < 0.1 and len(img)-1 != i else img[i] for i in range(len(img))])
    return result


def test(network, path):
    imgs = parse_imgs(path)
    results = []
    deltas = []
    for img in imgs:
        result = predict(network, img)
        # print(img)
        print(result)
        print(img)
        print([network[0][i]['weights'] for i in range(len(network[0]))])
        print("-------------------------------------------------")
        results.append(result)
        # deltas.append(delta)
    return imgs, results, [network[0][i]['weights'] for i in range(len(network[0]))]


train_imgs = random_swap(parse_imgs(training_set))
inputs = len(train_imgs[0]) - 1

# outputs = len(set([row[-1] for row in train_imgs]))

network = init(inputs, network_neurons, inputs)
print(eta)
train(network, train_imgs, eta, max_epochs, inputs)

imgs, results, hidden_layer = test(network, "nums_small")
n = 10
for i in range(n):
    # display original
    ax = plt.subplot(3, n, i + 1)
    imgs[i].pop()
    plt.imshow(np.reshape(imgs[i], (10, 7)))
    plt.gray()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

    # display reconstruction
    ax = plt.subplot(3, n, i + 1 + n)
    plt.imshow(np.reshape(results[i], (10, 7)))
    plt.gray()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

    # display delta
    ax = plt.subplot(3, n, i + 1 + 2 * n)
    hidden_layer[i].pop()

    # weights = [(value - min(deltas[i])) / (max(deltas[i]) - min(deltas[i])) for value in deltas[i]]
    # sum_weights = np.sqrt(sum(weight**2 for weight in weights))
    # img = [weight / sum_weights for weight in weights]
    weights = hidden_layer[i]
    sum_weights = np.sqrt(sum(weight**2 for weight in weights))
    values = [weight / sum_weights for weight in weights]
    # img = [(value - min(values)) / (max(values) - min(values)) for value in values]
    img = values
    # print(img)
    # img = deltas[i]
    plt.imshow(np.reshape(img, (10, 7)))
    plt.gray()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

plt.show()
# for row in train_imgs:
#     print("{}:{}".format(predict(network, row), row[-1]))

for layer in network:
    print(layer)



