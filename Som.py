import random
import numpy as np
import math
import matplotlib.pyplot as plt

TRAIN_CONST = 0.5
CITIES = 30
NODES = int((CITIES * 3))
TIME_CONST = 2500.0
MAP_RADIUS = 5

stop_at_start = True

def plot_step(som, inputs):
    i_x = [input[0] for input in inputs]
    i_y = [input[1] for input in inputs]

    s_x = [node[0] for node in som]
    s_x.append(som[0][0])
    s_y = [node[1] for node in som]
    s_y.append(som[0][1])
    plt.clf()
    plt.plot(i_x, i_y, 'r*', markersize = '20')
    plt.plot(s_x, s_y, 'b-*')
    plt.pause(0.01)
    global stop_at_start
    if stop_at_start:
        plt.waitforbuttonpress()
        stop_at_start = False


def init_som(nodes_amount, inputs_amount):
    # outside
    # som = [[random.uniform(1.0, 2.0) for i in range(inputs_amount)] for i in range(nodes_amount)]
    # som = [[random.uniform(0.0, 1.0) for i in range(inputs_amount)] for i in range(nodes_amount)]
    # som = [[0.5 for i in range(inputs_amount)] for i in range(nodes_amount)]
    # som = [[random.uniform(0.0, 0.5) for i in range(inputs_amount)] for i in range(nodes_amount)]
    som = [[math.cos(2*math.pi/nodes_amount*i)*0.2 + 0.5,math.sin(2*math.pi/nodes_amount*i)*0.2 + 0.5] for i in range(nodes_amount)]
    return som


def random_input(inputs_amount, train_set_size):
    inputs = [[random.uniform(0.0, 1.0) for i in range(inputs_amount)] for j in range(train_set_size)]
    return inputs


def dist_fun_travelling(node_index, winner_index):
    minimum = min(node_index, winner_index)
    maximum = max(node_index, winner_index)
    return min(maximum - minimum, NODES + minimum - maximum)


def pick_winner(input, som):
    winner = []
    winner_value = np.inf
    for node in som:
        value = np.sqrt(sum((node[i] - input[i])**2 for i in range(len(node))))
        if winner_value > value:
            winner = node
            winner_value = value
    return winner, som.index(winner)


def near_fun(dist_from_best, epoch):
    radius = MAP_RADIUS * max(np.exp(-epoch / TIME_CONST), 0.2)
    # radius = MAP_RADIUS * np.exp(-epoch / TIME_CONST)
    temp = np.exp((-1 * (dist_from_best ** 2)) / (2 * radius * radius))
    return temp


def learning_rate(epoch):
    rate = min(TRAIN_CONST / np.sqrt(epoch), 0.1)
    return rate


def update(som, input, epoch, winner_index):
    for node in som:
        node_index = som.index(node)
        dist_from_best = dist_fun_travelling(node_index, winner_index)
        for j in range(len(node)):
            node[j] += near_fun(dist_from_best, epoch) * \
                       learning_rate(epoch) * (input[j] - node[j])


def apply_inputs(input, som, epoch):
    winner, winner_index = pick_winner(input, som)
    update(som, input, epoch, winner_index)
    if epoch % 100 == 0 or epoch == 1:
        plot_step(som, inputs)


def train(som, inputs):
    # map_radius = 0.5
    counter = 1
    # for i in range(epochs):
    while True:
        for input in random.sample(inputs, 1):
            apply_inputs(input, som, counter)
            counter += 1
            print("epoch:%d" % counter)


# inputs = [
#     [4.9, 4.9],
#     [4.7, 4.9],
#     [4.9, 4.8],
#     [0.1, 0.1],
#     [0.1, 0.2],
#     [0.2, 0.1],
#     [4.9, 0.1],
#     [4.9, 0.2],
#     [4.7, 0.1],
#     [0.1, 2.9],
#     [0.2, 2.9],
#     [0.1, 2.7]
# ]

inputs = random_input(2, CITIES)
som = init_som(NODES, len(inputs[0]))

train(som, inputs)
